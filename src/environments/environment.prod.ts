export const environment = {
    production: true,
    apis: {
        googleBooks: {
            url: 'https://www.googleapis.com/books/v1',
            apiKey: 'AIzaSyC6ehqwLTRCiDIPyP3M8vBMrzcliqPZPyA',
            clientId: '962356278208-gifb6nf56cso8imp724bqhg4i8kicrgc.apps.googleusercontent.com',
            scope: 'https://www.googleapis.com/auth/books'
        }
    }
};
