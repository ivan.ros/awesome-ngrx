import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-errors-page',
    templateUrl: './errors-page.component.html',
    styleUrls: ['./errors-page.component.scss']
})
export class ErrorsPageComponent implements OnInit {
    public redirectUrl: string;

    constructor(public router: Router, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.queryParams.subscribe(params => {
            this.redirectUrl = params.redirect;
        });
    }
}
