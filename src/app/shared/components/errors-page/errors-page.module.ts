import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorsPageComponent } from './containers/errors-page.component';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material';

const routes = [
    {
        path: '',
        component: ErrorsPageComponent
    }
];

@NgModule({
    imports: [CommonModule, RouterModule.forChild(routes), MatButtonModule],
    exports: [ErrorsPageComponent],
    declarations: [ErrorsPageComponent]
})
export class ErrorsPageModule {}
