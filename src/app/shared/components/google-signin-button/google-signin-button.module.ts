import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { GoogleAuthService } from '@core/services/google-auth/google-auth.service';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { GoogleSigninButtonComponent } from './containers/google-signin-button.component';
import { UserEffects } from './ngrx-store/effects';
import { userReducer } from './ngrx-store/reducer';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        MatButtonModule,
        StoreModule.forFeature('user', userReducer),
        EffectsModule.forFeature([UserEffects])
    ],
    exports: [GoogleSigninButtonComponent],
    providers: [GoogleAuthService],
    declarations: [GoogleSigninButtonComponent]
})
export class GoogleSigninButtonModule {}
