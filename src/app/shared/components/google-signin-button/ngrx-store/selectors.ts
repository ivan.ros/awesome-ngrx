import { User } from '@core/models/user';
import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import { State } from './state';

const getUser = (state: State): any => state.user;

export const selectUserState: MemoizedSelector<object, State> = createFeatureSelector<State>('user');

export const selectUserCurrentUser: MemoizedSelector<object, User> = createSelector(selectUserState, getUser);
