import { User } from '@core/models/user';
import { Action } from '@ngrx/store';

export enum ActionTypes {
    INIT_GOOGLE_API = '[User] Init Google API',
    INIT_GOOGLE_API_SUCCESS = '[User] Init Google API Success',
    GET_CURRENT_USER = '[User] Get User Request',
    GET_CURRENT_USER_SUCCESS = '[User] Get User Success'
}

export class InitGoogleApiAction implements Action {
    readonly type = ActionTypes.INIT_GOOGLE_API;
    constructor(public params: { clientId: string; scope: string }) {}
}

export class InitGoogleApiSuccessAction implements Action {
    readonly type = ActionTypes.INIT_GOOGLE_API_SUCCESS;
    constructor() {}
}

export class GetCurrentUserAction implements Action {
    readonly type = ActionTypes.GET_CURRENT_USER;
    constructor(public params: { element: any }) {}
}

export class GetCurrentUserSuccessAction implements Action {
    readonly type = ActionTypes.GET_CURRENT_USER_SUCCESS;
    constructor(public payload: { user: User }) {}
}

export type Actions =
    | InitGoogleApiAction
    | InitGoogleApiSuccessAction
    | GetCurrentUserAction
    | GetCurrentUserSuccessAction;
