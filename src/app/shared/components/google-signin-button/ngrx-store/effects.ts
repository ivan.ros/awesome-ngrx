import { Injectable } from '@angular/core';
import { User } from '@core/models/user';
import { GoogleAuthService } from '@core/services/google-auth/google-auth.service';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import * as featureActions from './actions';

@Injectable()
export class UserEffects {
    constructor(private googleAuthService: GoogleAuthService, private actions$: Actions) {}

    @Effect()
    initGoogleApiEffect: Observable<Action> = this.actions$.pipe(
        ofType<featureActions.InitGoogleApiAction>(featureActions.ActionTypes.INIT_GOOGLE_API),
        switchMap(actions =>
            this.googleAuthService.init(actions.params.clientId, actions.params.scope).pipe(
                map(() => {
                    return new featureActions.InitGoogleApiSuccessAction();
                })
            )
        )
    );

    @Effect()
    getCurrentUserEffect$: Observable<Action> = this.actions$.pipe(
        ofType<featureActions.GetCurrentUserAction>(featureActions.ActionTypes.GET_CURRENT_USER),
        switchMap(actions =>
            this.googleAuthService.computeGoogleSignInElement(actions.params.element).pipe(
                map(() => {
                    const user: User = this.googleAuthService.getBasicProfile();
                    return new featureActions.GetCurrentUserSuccessAction({
                        user
                    });
                })
            )
        )
    );
}
