import { User } from '@core/models/user';

export interface State {
    user: User;
}

export const initialState: State = {
    user: null
};
