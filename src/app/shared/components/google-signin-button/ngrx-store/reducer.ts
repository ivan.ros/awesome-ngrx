import { Actions, ActionTypes } from './actions';
import { initialState, State } from './state';

export function userReducer(state = initialState, action: Actions): State {
    switch (action.type) {
        case ActionTypes.INIT_GOOGLE_API: {
            return {
                ...state
            };
        }
        case ActionTypes.INIT_GOOGLE_API_SUCCESS: {
            return {
                ...state
            };
        }
        case ActionTypes.GET_CURRENT_USER: {
            return {
                ...state
            };
        }
        case ActionTypes.GET_CURRENT_USER_SUCCESS: {
            return {
                ...state,
                user: action.payload.user
            };
        }
        default: {
            return state;
        }
    }
}
