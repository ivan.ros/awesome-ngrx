import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { User } from '@core/models/user';
import { GoogleAuthService } from '@core/services/google-auth/google-auth.service';
import { select, Store } from '@ngrx/store';
import * as FeatureActions from '@shared/components/google-signin-button/ngrx-store/actions';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import * as RootStoreSelectors from '../../../../root-store/selectors';
import * as RootStoreState from '../../../../root-store/state';

@Component({
    selector: 'app-google-signin-button',
    templateUrl: './google-signin-button.component.html',
    styleUrls: ['./google-signin-button.component.scss']
})
export class GoogleSigninButtonComponent implements AfterViewInit {
    @ViewChild('googleBtn') googleBtn: ElementRef;

    public currentUser$: Observable<User> = null;

    constructor(
        private element: ElementRef,
        private googleAuthService: GoogleAuthService,
        private store$: Store<RootStoreState.State>
    ) {
        const params: { clientId; scope } = environment.apis.googleBooks;
        this.store$.dispatch(new FeatureActions.InitGoogleApiAction(params));
    }

    ngAfterViewInit() {
        this.currentUser$ = this.store$.pipe(select(RootStoreSelectors.selectUserCurrentUser));

        this.store$.dispatch(new FeatureActions.GetCurrentUserAction({ element: this.element.nativeElement }));
    }
}
