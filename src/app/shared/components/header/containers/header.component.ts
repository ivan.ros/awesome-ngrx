import { Component, Input, OnInit } from '@angular/core';
import { MenuItem } from 'src/app/core/models/menu-item';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    @Input()
    menuItems: Array<MenuItem>;

    public userInfo = null;

    constructor() {}

    ngOnInit() {}
}
