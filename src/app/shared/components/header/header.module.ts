import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './containers/header.component';
import { GoogleSigninButtonModule } from '../google-signin-button/google-signin-button.module';

@NgModule({
    imports: [CommonModule, RouterModule, MatButtonModule, MatMenuModule, MatToolbarModule, GoogleSigninButtonModule],
    exports: [HeaderComponent],
    declarations: [HeaderComponent]
})
export class HeaderModule {}
