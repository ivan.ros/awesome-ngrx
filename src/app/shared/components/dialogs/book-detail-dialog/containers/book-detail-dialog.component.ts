import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Volume } from '@core/models/volume';
import * as FeatureActions from '@features/all-books/ngrx-store/actions';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as RootStoreSelectors from '../../../../../root-store/selectors';
import * as RootStoreState from '../../../../../root-store/state';

@Component({
    selector: 'app-book-detail-dialog',
    templateUrl: './book-detail-dialog.component.html',
    styleUrls: ['./book-detail-dialog.component.scss']
})
export class BookDetailDialogComponent implements OnInit {
    public bookTmp: Volume;

    public book$: Observable<Volume>;
    public error$: Observable<string>;
    public isLoading$: Observable<boolean>;

    constructor(@Inject(MAT_DIALOG_DATA) private data, private store$: Store<RootStoreState.State>) {
        this.bookTmp = data.volume;
    }

    ngOnInit() {
        this.book$ = this.store$.pipe(select(RootStoreSelectors.selectBook));
        this.error$ = this.store$.pipe(select(RootStoreSelectors.selectBookError));
        this.isLoading$ = this.store$.pipe(select(RootStoreSelectors.selectBookIsLoading));

        this.store$.dispatch(
            new FeatureActions.GetBookAction({
                volumeId: this.data.volume.id
            })
        );
    }
}
