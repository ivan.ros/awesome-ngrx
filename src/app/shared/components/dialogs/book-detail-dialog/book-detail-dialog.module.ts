import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookDetailDialogComponent } from './containers/book-detail-dialog.component';
import { MatDialogModule, MatProgressSpinnerModule } from '@angular/material';

const components = [BookDetailDialogComponent];

@NgModule({
    declarations: [...components],
    imports: [CommonModule, MatDialogModule, MatProgressSpinnerModule],
    exports: [CommonModule, ...components],
    entryComponents: [...components]
})
export class BookDetailDialogModule {}
