import { Component, Input } from '@angular/core';
import { Volume } from 'src/app/core/models/volume';
import { MatDialog } from '@angular/material';
import { BookDetailDialogComponent } from '../../dialogs/book-detail-dialog/containers/book-detail-dialog.component';

@Component({
    selector: 'app-books',
    templateUrl: './books.component.html',
    styleUrls: ['./books.component.scss']
})
export class BooksComponent {
    @Input()
    books: Array<Volume>;

    constructor(private matDialog: MatDialog) {}

    openVolumeInfoDialog(volume: Volume) {
        this.matDialog.open(BookDetailDialogComponent, { width: '700px', data: { volume } });
    }
}
