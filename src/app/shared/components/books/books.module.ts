import { NgModule } from '@angular/core';
import { BooksComponent } from './containers/books.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material';
import { BookDetailDialogModule } from '../dialogs/book-detail-dialog/book-detail-dialog.module';

@NgModule({
    imports: [CommonModule, FormsModule, MatDialogModule, BookDetailDialogModule],
    exports: [BooksComponent],
    declarations: [BooksComponent]
})
export class BooksModule {}
