import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GoogleAuthGuard } from './core/guards/google-auth/google-auth.guard';

const routes: Routes = [
    {
        path: '',
        loadChildren: './features/main-page/main-page.module#MainPageModule'
    },
    {
        path: 'counter',
        loadChildren: './features/counter/counter.module#CounterModule'
    },
    {
        path: 'all-books',
        loadChildren: './features/all-books/all-books.module#AllBooksModule'
    },
    {
        path: 'my-books',
        loadChildren: './features/my-books/my-books.module#MyBooksModule',
        canActivate: [GoogleAuthGuard]
    },
    {
        path: 'error',
        loadChildren: './shared/components/errors-page/errors-page.module#ErrorsPageModule'
    },
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
