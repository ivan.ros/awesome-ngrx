import { Component } from '@angular/core';
import { MenuItem } from './core/models/menu-item';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    public menuItems: Array<MenuItem> = [
        {
            name: 'Basics',
            children: [
                {
                    name: 'Counter',
                    route: 'counter'
                }
            ]
        },
        {
            name: 'Books',
            children: [
                {
                    name: 'All Books',
                    route: 'all-books'
                },
                {
                    name: 'My Books',
                    route: 'my-books'
                }
            ]
        }
    ];
}
