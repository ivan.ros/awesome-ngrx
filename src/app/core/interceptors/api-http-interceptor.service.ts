import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ApiHttpInterceptor implements HttpInterceptor {
    constructor() {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            catchError((response: HttpErrorResponse) => {
                switch (response.status) {
                    case 400:
                        const err =
                            Array.isArray(response.error) && response.error.length > 1
                                ? '<ul><li>' + response.error.join('</li><li>') + '</li></ul>'
                                : response.error[0];
                        this.handleError(err);
                        break;
                    default:
                        this.handleError(JSON.stringify(response.error));
                }

                return throwError(response);
            })
        );
    }

    public handleError(error: string) {
        console.error(error);
    }
}
