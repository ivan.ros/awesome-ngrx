export class Bookshelf {
    kind: string;
    id: number;
    selfLink: string;
    title: string;
    description: string;
    access: string;
    updated: Date;
    created: Date;
    volumeCount: number;
    volumesLastUpdated: Date;
}
