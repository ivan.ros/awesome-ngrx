export class User {
    id: string;
    token: string;
    name: string;
    imageUrl: string;
    email: string;
}
