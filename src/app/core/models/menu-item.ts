export class MenuItem {
    name: string;
    route?: string;
    children?: Array<MenuItem>;
    id?: string;
}
