import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { select, Store } from '@ngrx/store';
import * as RootStoreSelectors from '../../../root-store/selectors';
import { Observable } from 'rxjs';
import * as RootStoreState from '../../../root-store/state';
import { map, take } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class GoogleAuthGuard implements CanActivate {
    constructor(private store$: Store<RootStoreState.State>, private router: Router) {}

    getIsLoggedInStatus(): Observable<any> {
        return this.store$.pipe(select(RootStoreSelectors.selectUserCurrentUser), take(1));
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.getIsLoggedInStatus().pipe(
            map(user => {
                const isLoggedIn = !!user;
                if (!isLoggedIn) {
                    this.router.navigate(['/error'], { queryParams: { redirect: next.url } });
                }
                return isLoggedIn;
            })
        );
    }
}
