import { TestBed, async, inject } from '@angular/core/testing';

import { GoogleAuthGuard } from './google-auth.guard';

describe('GoogleAuthGuard', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [GoogleAuthGuard]
        });
    });

    it('should ...', inject([GoogleAuthGuard], (guard: GoogleAuthGuard) => {
        expect(guard).toBeTruthy();
    }));
});
