import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../../models/user';

declare const window: any;

@Injectable()
export class GoogleAuthService {
    public apiKey: string;
    public googleAuth: any;
    public googleUser: any = new Subject();
    public window = window;

    constructor() {}

    /**
     * Init OAuth2 with provided Google's ApiKey.
     * @param key Api key
     * @param scope Api scope
     */
    public init(key: string, scope?: string): Observable<any> {
        this.apiKey = key;
        return Observable.create(
            this.window.gapi.load('auth2', () => {
                this.googleAuth = this.window.gapi.auth2.init({
                    client_id: this.apiKey,
                    scope
                });
            })
        );
    }

    /**
     * Check if an User is already registered.
     * If it's the case, then go to the method who will dispatch the google-auth.
     * @returns Observable Service response as an observable
     */
    public isLoggedIn(): Observable<boolean> {
        return Observable.create((observer: any) => {
            this.isAvailable().then(() => {
                this.googleAuth.isSignedIn.listen((isLogged: boolean) => {
                    if (isLogged) {
                        this.dispatchGoogleUser();
                    }
                    observer.next(this.googleAuth.isSignedIn.get());
                });
            });
        });
    }

    /**
     * Render a custom element for GoogleSignIn button.
     * Then attach an handler && return the response.
     * @param domElement DOM Element
     * @returns Observable Service response as an observable
     */
    public computeGoogleSignInElement(domElement: any): Observable<any> {
        return Observable.create((observer: any) => {
            this.isAvailable().then(() => {
                return this.googleAuth.attachClickHandler(
                    domElement,
                    {},
                    (googleUser: any) => {
                        this.setGoogleUser(googleUser);
                        observer.next(googleUser);
                    },
                    (error: any) => {
                        observer.errorBooks(alert(JSON.stringify(error, undefined, 2)));
                    }
                );
            });
        });
    }

    /**
     * Reload the token.
     * @returns Observable Service response as an observable
     */
    public reloadAuthResponse(): Observable<any> {
        return Observable.create((observer: any) => {
            this.getCurrentUser()
                .get()
                .reloadAuthResponse()
                .then((data: any) => observer.next(data));
        });
    }

    /**
     * Get AuthResponse.
     * @param includeAuthorizationData If include auth data or not
     * @returns Observable Service response as an observable
     */
    public getAuthResponse(includeAuthorizationData?: boolean): Observable<any> {
        return Observable.create((observer: any) => {
            observer.next(
                this.getCurrentUser()
                    .get()
                    .getAuthResponse(includeAuthorizationData)
            );
        });
    }

    /**
     * Check if the google-auth a the rights specified in the scopes.
     * @param scopes Google roles to use
     * @returns Observable Service response as an observable
     */
    public hasGrantedScopes(scopes: string): Observable<boolean> {
        return Observable.create((observer: any) => {
            observer.next(
                this.getCurrentUser()
                    .get()
                    .hasGrantedScopes(scopes)
            );
        });
    }

    /**
     * Expose GoogleUser.
     */
    public getCurrentUser(): any {
        return this.googleAuth.currentUser;
    }

    /**
     * Expose BasicProfile information.
     * @returns User User parsed data
     */
    public getBasicProfile(): User {
        const profile = this.getCurrentUser()
            .get()
            .getBasicProfile();

        return {
            id: profile.getId(),
            token: this.googleUser.id_token,
            name: profile.getName(),
            imageUrl: profile.getImageUrl(),
            email: profile.getEmail()
        };
    }

    /**
     * Retrieves google user data.
     */
    public getGoogleUser() {
        return this.googleUser;
    }

    /**
     * Logout.
     */
    public signOut() {
        this.googleAuth.signOut();
    }

    /**
     * Disconnect.
     */
    public disconnect() {
        this.googleAuth.disconnect();
    }

    /**
     * Appends Google API key to HttpParams
     * @param params Modified HttpParams
     */
    public appendApiKeyToParams(params: HttpParams): HttpParams {
        return params.append('key', environment.apis.googleBooks.apiKey);
    }

    /**
     * Dispatch the google google-auth across the application.
     */
    private dispatchGoogleUser(): void {
        this.setGoogleUser(this.googleAuth.isSignedIn.get());
    }

    /**
     * Emit the current google-auth object.
     * @param googleUser Google user data
     */
    private setGoogleUser(googleUser: any): any {
        this.googleUser.next(this.getCurrentUser().get());
    }

    /**
     * Helpers.
     */
    private isAvailable() {
        return new Promise(resolve => {
            this.window.gapi.load('auth2', () => {
                resolve(true);
            });
        });
    }
}
