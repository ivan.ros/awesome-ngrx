import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Bookshelf } from '../../models/bookshelf';
import { Volume } from '../../models/volume';
import { ConfigService } from '../config/config.service';
import { GoogleAuthService } from '../google-auth/google-auth.service';

@Injectable({
    providedIn: 'root'
})
export class VolumesService {
    constructor(
        private httpClient: HttpClient,
        private config: ConfigService,
        private googleAuthService: GoogleAuthService
    ) {}

    public getVolume(volumeId: string): Observable<Volume> {
        return this.httpClient.get<Volume>(this.config.endpoints.books.volume.replace(':volumeId', volumeId));
    }

    public getAllVolumes(search: string): Observable<Array<Volume>> {
        let params = new HttpParams();
        params = params.append('q', search);

        return this.httpClient
            .get<{ items: Array<Volume> }>(this.config.endpoints.books.allVolumes, { params })
            .pipe(map(response => response.items || []));
    }

    public getMyBookshelves(): Observable<Array<Bookshelf>> {
        let params = new HttpParams();
        params = this.googleAuthService.appendApiKeyToParams(params);

        return this.httpClient
            .get<{ items: Array<Bookshelf> }>(this.config.endpoints.books.myBookshelves, { params })
            .pipe(map(response => response.items || []));
    }

    public getMyBookshelfVolumes(bookshelf: Bookshelf, search: string): Observable<Array<Volume>> {
        let params = new HttpParams();
        params = params.append('q', search);

        return this.httpClient
            .get<{ items: Array<Volume> }>(
                this.config.endpoints.books.myBookshelfVolumes.replace(':shelf', bookshelf.id.toString()),
                { params }
            )
            .pipe(map(response => response.items || []));
    }
}
