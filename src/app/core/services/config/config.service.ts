import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ConfigService {
    googleBooksApiUrl = environment.apis.googleBooks.url;

    endpoints = {
        books: {
            volume: `${this.googleBooksApiUrl}/volumes/:volumeId`,
            allVolumes: `${this.googleBooksApiUrl}/volumes`,
            myBookshelves: `${this.googleBooksApiUrl}/mylibrary/bookshelves`,
            myBookshelfVolumes: `${this.googleBooksApiUrl}/mylibrary/bookshelves/:shelf/volumes`
        }
    };

    constructor() {}
}
