import { TestBed } from '@angular/core/testing';
import { ConfigService } from './config.service';

describe('GIVEN ConfigService', () => {
    beforeEach(() => TestBed.configureTestingModule({}).compileComponents());

    it('SHOULD be created', () => {
        const service: ConfigService = TestBed.get(ConfigService);
        expect(service).toBeTruthy();
    });
});
