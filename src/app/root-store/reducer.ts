import { allBooksReducer } from '@features/all-books/ngrx-store/reducer';
import { counterReducer } from '@features/counter/ngrx-store/reducer';
import { myBooksReducer } from '@features/my-books/ngrx-store/reducer';
import { ActionReducerMap } from '@ngrx/store';
import { userReducer } from '@shared/components/google-signin-button/ngrx-store/reducer';
import { State } from './state';

export const reducers: ActionReducerMap<State> = {
    counter: counterReducer,
    user: userReducer,
    allBooks: allBooksReducer,
    myBooks: myBooksReducer
};
