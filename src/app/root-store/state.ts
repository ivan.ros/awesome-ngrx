import * as AllBooksState from '@features/all-books/ngrx-store/state';
import * as MyBooksState from '@features/my-books/ngrx-store/state';
import * as UserState from '@shared/components/google-signin-button/ngrx-store/state';

export interface State {
    counter: number;
    user: UserState.State;
    allBooks: AllBooksState.State;
    myBooks: MyBooksState.State;
}
