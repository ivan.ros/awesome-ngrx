import { createFeatureSelector } from '@ngrx/store';
export * from '@features/all-books/ngrx-store/selectors';
export * from '@features/my-books/ngrx-store/selectors';
export * from '@shared/components/google-signin-button/ngrx-store/selectors';

export const getCounterState = createFeatureSelector<number>('counter');
export const getUserState = createFeatureSelector<any>('user');
export const getAllBooksState = createFeatureSelector<any>('allBooks');
export const getMyBooksState = createFeatureSelector<any>('myBooks');
