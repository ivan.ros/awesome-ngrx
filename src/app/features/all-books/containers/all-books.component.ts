import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Volume } from 'src/app/core/models/volume';
import * as RootStoreSelectors from '../../../root-store/selectors';
import * as RootStoreState from '../../../root-store/state';
import * as FeatureActions from '../ngrx-store/actions';

@Component({
    selector: 'app-all-books',
    templateUrl: './all-books.component.html',
    styleUrls: ['./all-books.component.scss']
})
export class AllBooksComponent implements OnInit {
    public searchFormControl = new FormControl('Sass', []);

    public books$: Observable<Array<Volume>>;
    public error$: Observable<string>;
    public isLoading$: Observable<boolean>;

    constructor(private store$: Store<RootStoreState.State>) {}

    ngOnInit() {
        this.books$ = this.store$.pipe(select(RootStoreSelectors.selectAllBooks));
        this.error$ = this.store$.pipe(select(RootStoreSelectors.selectAllBooksError));
        this.isLoading$ = this.store$.pipe(select(RootStoreSelectors.selectAllBooksIsLoading));

        this.store$.dispatch(
            new FeatureActions.GetAllBooksAction({
                search: this.searchFormControl.value
            })
        );
    }
}
