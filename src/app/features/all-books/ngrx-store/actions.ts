import { Action } from '@ngrx/store';
import { Volume } from 'src/app/core/models/volume';

export enum ActionTypes {
    GET_BOOK = '[AllBooks] Get Book Request',
    GET_BOOK_SUCCESS = '[AllBooks] Get Book Success',
    GET_BOOK_FAILURE = '[AllBooks] Get Book Failure',
    GET_ALL_BOOKS = '[AllBooks] Get All Books Request',
    GET_ALL_BOOKS_SUCCESS = '[AllBooks] Get All Books Success',
    GET_ALL_BOOKS_FAILURE = '[AllBooks] Get All Book Failure'
}

export class GetBookAction implements Action {
    readonly type = ActionTypes.GET_BOOK;
    constructor(public params: { volumeId: string }) {}
}

export class GetBookSuccessAction implements Action {
    readonly type = ActionTypes.GET_BOOK_SUCCESS;
    constructor(public payload: { book: Volume }) {}
}

export class GetBookFailureAction implements Action {
    readonly type = ActionTypes.GET_BOOK_FAILURE;
    constructor(public payload: { error: string }) {}
}

export class GetAllBooksAction implements Action {
    readonly type = ActionTypes.GET_ALL_BOOKS;
    constructor(public params: { search: string }) {}
}

export class GetAllBooksSuccessAction implements Action {
    readonly type = ActionTypes.GET_ALL_BOOKS_SUCCESS;
    constructor(public payload: { books: Array<Volume> }) {}
}

export class GetAllBooksFailureAction implements Action {
    readonly type = ActionTypes.GET_ALL_BOOKS_FAILURE;
    constructor(public payload: { error: string }) {}
}

export type Actions =
    | GetBookAction
    | GetBookSuccessAction
    | GetBookFailureAction
    | GetAllBooksAction
    | GetAllBooksSuccessAction
    | GetAllBooksFailureAction;
