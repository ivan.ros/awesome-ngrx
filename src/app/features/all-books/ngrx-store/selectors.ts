import { Volume } from '@core/models/volume';
import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import { State } from './state';

const getErrorBook = (state: State): any => state.errorBook;

const getIsLoadingBook = (state: State): boolean => state.isLoadingBook;

const getBook = (state: State): any => state.book;

const getErrorBooks = (state: State): any => state.errorBooks;

const getIsLoadingBooks = (state: State): boolean => state.isLoadingBooks;

const getBooks = (state: State): any => state.books;

export const selectAllBooksState: MemoizedSelector<object, State> = createFeatureSelector<State>('allBooks');

export const selectBookError: MemoizedSelector<object, any> = createSelector(selectAllBooksState, getErrorBook);

export const selectBookIsLoading: MemoizedSelector<object, boolean> = createSelector(
    selectAllBooksState,
    getIsLoadingBook
);

export const selectBook: MemoizedSelector<object, Volume> = createSelector(selectAllBooksState, getBook);

export const selectAllBooksError: MemoizedSelector<object, any> = createSelector(selectAllBooksState, getErrorBooks);

export const selectAllBooksIsLoading: MemoizedSelector<object, boolean> = createSelector(
    selectAllBooksState,
    getIsLoadingBooks
);

export const selectAllBooks: MemoizedSelector<object, Array<Volume>> = createSelector(selectAllBooksState, getBooks);
