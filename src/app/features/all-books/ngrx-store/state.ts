import { Volume } from '@core/models/volume';

export interface State {
    book: Volume;
    isLoadingBook: boolean;
    errorBook: string;
    books: Array<Volume>;
    isLoadingBooks: boolean;
    errorBooks: string;
}

export const initialState: State = {
    book: null,
    isLoadingBook: false,
    errorBook: null,
    books: [],
    isLoadingBooks: false,
    errorBooks: null
};
