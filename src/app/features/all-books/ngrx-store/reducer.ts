import { Actions, ActionTypes } from './actions';
import { initialState, State } from './state';

export function allBooksReducer(state = initialState, action: Actions): State {
    switch (action.type) {
        case ActionTypes.GET_BOOK: {
            return {
                ...state,
                isLoadingBook: true,
                errorBook: null
            };
        }
        case ActionTypes.GET_BOOK_SUCCESS: {
            return {
                ...state,
                book: action.payload.book,
                isLoadingBook: false,
                errorBook: null
            };
        }
        case ActionTypes.GET_BOOK_FAILURE: {
            return {
                ...state,
                isLoadingBook: false,
                errorBook: action.payload.error
            };
        }
        case ActionTypes.GET_ALL_BOOKS: {
            return {
                ...state,
                isLoadingBooks: true,
                errorBooks: null
            };
        }
        case ActionTypes.GET_ALL_BOOKS_SUCCESS: {
            return {
                ...state,
                books: action.payload.books,
                isLoadingBooks: false,
                errorBooks: null
            };
        }
        case ActionTypes.GET_ALL_BOOKS_FAILURE: {
            return {
                ...state,
                isLoadingBooks: false,
                errorBooks: action.payload.error
            };
        }
        default: {
            return state;
        }
    }
}
