import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of as observableOf } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { VolumesService } from 'src/app/core/services/volumes/volumes.service';
import * as featureActions from './actions';

@Injectable()
export class AllBooksEffects {
    constructor(private volumesService: VolumesService, private actions$: Actions) {}

    @Effect()
    getBookEffect$: Observable<Action> = this.actions$.pipe(
        ofType<featureActions.GetBookAction>(featureActions.ActionTypes.GET_BOOK),
        switchMap(action =>
            this.volumesService.getVolume(action.params.volumeId).pipe(
                map(
                    book =>
                        new featureActions.GetBookSuccessAction({
                            book
                        })
                ),
                catchError(error => observableOf(new featureActions.GetBookFailureAction({ error })))
            )
        )
    );

    @Effect()
    getAllBooksEffect$: Observable<Action> = this.actions$.pipe(
        ofType<featureActions.GetAllBooksAction>(featureActions.ActionTypes.GET_ALL_BOOKS),
        switchMap(action =>
            this.volumesService.getAllVolumes(action.params.search).pipe(
                map(
                    books =>
                        new featureActions.GetAllBooksSuccessAction({
                            books
                        })
                ),
                catchError(error => observableOf(new featureActions.GetAllBooksFailureAction({ error })))
            )
        )
    );
}
