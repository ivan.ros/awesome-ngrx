import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { BooksModule } from '@shared/components/books/books.module';
import { AllBooksComponent } from './containers/all-books.component';
import { AllBooksEffects } from './ngrx-store/effects';
import { allBooksReducer } from './ngrx-store/reducer';

const routes = [
    {
        path: '',
        component: AllBooksComponent
    }
];

@NgModule({
    declarations: [AllBooksComponent],
    exports: [AllBooksComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        BooksModule,
        RouterModule.forChild(routes),
        StoreModule.forFeature('allBooks', allBooksReducer),
        EffectsModule.forFeature([AllBooksEffects]),
        MatProgressSpinnerModule,
        MatFormFieldModule,
        MatInputModule
    ]
})
export class AllBooksModule {}
