import { createAction, props } from '@ngrx/store';

export const increment = createAction('[Counter] Increment Counter');

export const decrement = createAction('[Counter] Decrement Counter');

export const random = createAction('[Counter] Generate Random Counter');

export const jump = createAction('[Counter] Jump Counter To Number', props<{ num: number }>());
