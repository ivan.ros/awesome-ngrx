import { Action, createReducer, on } from '@ngrx/store';
import * as CounterActions from './actions';

export const featureKey = 'counter';
export const initialState = 0;

const _counterReducer = createReducer(
    initialState,
    on(CounterActions.increment, state => state + 1),
    on(CounterActions.decrement, state => state - 1),
    on(CounterActions.random, state => Math.floor(Math.random() * 100)),
    on(CounterActions.jump, (state, { num }) => num)
);

export function counterReducer(state: number | undefined, action: Action) {
    return _counterReducer(state, action);
}
