import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as RootStoreState from '../../../root-store/state';
import * as actions from '../ngrx-store/actions';

@Component({
    selector: 'app-counter',
    templateUrl: './counter.component.html',
    styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {
    public count$: Observable<number>;
    public jumpNumberFormControl = new FormControl(0, []);

    constructor(private store: Store<RootStoreState.State>) {}

    ngOnInit() {
        this.count$ = this.store.pipe(select('counter'));
    }

    public increment() {
        this.store.dispatch(actions.increment());
    }

    public decrement() {
        this.store.dispatch(actions.decrement());
    }

    public random() {
        this.store.dispatch(actions.random());
    }

    public jumpTo(n: number) {
        this.store.dispatch(actions.jump({ num: +n }));
    }
}
