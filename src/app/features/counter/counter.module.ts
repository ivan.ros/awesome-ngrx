import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { CounterComponent } from './containers/counter.component';
import * as fromCounter from './ngrx-store/reducer';

const routes = [
    {
        path: '',
        component: CounterComponent
    }
];

@NgModule({
    declarations: [CounterComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        StoreModule.forFeature(fromCounter.featureKey, fromCounter.counterReducer),
        MatButtonModule,
        MatInputModule
    ]
})
export class CounterModule {}
