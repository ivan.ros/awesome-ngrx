import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Bookshelf } from '@core/models/bookshelf';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Volume } from 'src/app/core/models/volume';
import * as RootStoreSelectors from '../../../root-store/selectors';
import * as RootStoreState from '../../../root-store/state';
import * as FeatureActions from '../ngrx-store/actions';

@Component({
    selector: 'app-my-books',
    templateUrl: './my-books.component.html',
    styleUrls: ['./my-books.component.scss']
})
export class MyBooksComponent implements OnInit {
    public searchFormControl = new FormControl('', []);

    public bookshelves$: Observable<Array<Bookshelf>>;
    public books$: Observable<Array<Volume>>;
    public error$: Observable<string>;
    public isLoading$: Observable<boolean>;

    public currentBookshelf: Bookshelf;

    constructor(private store$: Store<RootStoreState.State>) {}

    ngOnInit() {
        this.bookshelves$ = this.store$.pipe(select(RootStoreSelectors.selectMyBooksBookshelves));
        this.books$ = this.store$.pipe(select(RootStoreSelectors.selectMyBooksBooks));
        this.error$ = this.store$.pipe(select(RootStoreSelectors.selectMyBooksError));
        this.isLoading$ = this.store$.pipe(select(RootStoreSelectors.selectMyBooksIsLoading));

        this.store$.dispatch(new FeatureActions.GetMyBookshelvesAction());
    }

    getBookshelfVolumes() {
        this.store$.dispatch(
            new FeatureActions.GetMyBooksAction({
                bookshelf: this.currentBookshelf,
                search: this.searchFormControl.value
            })
        );
    }
}
