import { Actions, ActionTypes } from './actions';
import { initialState, State } from './state';

export function myBooksReducer(state = initialState, action: Actions): State {
    switch (action.type) {
        case ActionTypes.GET_MY_BOOKSHELVES: {
            return {
                ...state,
                isLoading: true,
                error: null
            };
        }
        case ActionTypes.GET_MY_BOOKSHELVES_SUCCESS: {
            return {
                ...state,
                bookshelves: action.payload.bookshelves,
                isLoading: false,
                error: null
            };
        }
        case ActionTypes.GET_MY_BOOKSHELVES_FAILURE: {
            return {
                ...state,
                isLoading: false,
                error: action.payload.error
            };
        }
        case ActionTypes.GET_MY_BOOKS: {
            return {
                ...state,
                isLoading: true,
                error: null
            };
        }
        case ActionTypes.GET_MY_BOOKS_SUCCESS: {
            return {
                ...state,
                books: action.payload.books,
                isLoading: false,
                error: null
            };
        }
        case ActionTypes.GET_MY_BOOKS_FAILURE: {
            return {
                ...state,
                isLoading: false,
                error: action.payload.error
            };
        }
        default: {
            return state;
        }
    }
}
