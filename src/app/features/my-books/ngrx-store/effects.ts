import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of as observableOf } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { VolumesService } from 'src/app/core/services/volumes/volumes.service';
import * as featureActions from './actions';

@Injectable()
export class MyBooksEffects {
    constructor(private volumesService: VolumesService, private actions$: Actions) {}

    @Effect()
    getMyBookshelvesEffect$: Observable<Action> = this.actions$.pipe(
        ofType<featureActions.GetMyBookshelvesAction>(featureActions.ActionTypes.GET_MY_BOOKSHELVES),
        switchMap(() =>
            this.volumesService.getMyBookshelves().pipe(
                map(
                    bookshelves =>
                        new featureActions.GetMyBookshelvesSuccessAction({
                            bookshelves
                        })
                ),
                catchError(error => observableOf(new featureActions.GetMyBookshelvesFailureAction({ error })))
            )
        )
    );

    @Effect()
    getMyBooksEffect$: Observable<Action> = this.actions$.pipe(
        ofType<featureActions.GetMyBooksAction>(featureActions.ActionTypes.GET_MY_BOOKS),
        switchMap(action =>
            this.volumesService.getMyBookshelfVolumes(action.params.bookshelf, action.params.search).pipe(
                map(
                    books =>
                        new featureActions.GetMyBooksSuccessAction({
                            books
                        })
                ),
                catchError(error => observableOf(new featureActions.GetMyBooksFailureAction({ error })))
            )
        )
    );
}
