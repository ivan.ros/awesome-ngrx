import { Bookshelf } from '@core/models/bookshelf';
import { Volume } from '@core/models/volume';
import { Action } from '@ngrx/store';

export enum ActionTypes {
    GET_MY_BOOKSHELVES = '[MyBooks] Get Bookshelf Request',
    GET_MY_BOOKSHELVES_SUCCESS = '[MyBooks] Get Bookshelf Success',
    GET_MY_BOOKSHELVES_FAILURE = '[MyBooks] Get Bookshelf Failure',
    GET_MY_BOOKS = '[MyBooks] Get My Books Request',
    GET_MY_BOOKS_SUCCESS = '[MyBooks] Get My Books Success',
    GET_MY_BOOKS_FAILURE = '[MyBooks] Get My Books Failure'
}

export class GetMyBookshelvesAction implements Action {
    readonly type = ActionTypes.GET_MY_BOOKSHELVES;
    constructor() {}
}

export class GetMyBookshelvesSuccessAction implements Action {
    readonly type = ActionTypes.GET_MY_BOOKSHELVES_SUCCESS;
    constructor(public payload: { bookshelves: Array<Bookshelf> }) {}
}

export class GetMyBookshelvesFailureAction implements Action {
    readonly type = ActionTypes.GET_MY_BOOKSHELVES_FAILURE;
    constructor(public payload: { error: string }) {}
}

export class GetMyBooksAction implements Action {
    readonly type = ActionTypes.GET_MY_BOOKS;
    constructor(public params: { bookshelf: Bookshelf; search: string }) {}
}

export class GetMyBooksSuccessAction implements Action {
    readonly type = ActionTypes.GET_MY_BOOKS_SUCCESS;
    constructor(public payload: { books: Array<Volume> }) {}
}

export class GetMyBooksFailureAction implements Action {
    readonly type = ActionTypes.GET_MY_BOOKS_FAILURE;
    constructor(public payload: { error: string }) {}
}

export type Actions =
    | GetMyBookshelvesAction
    | GetMyBookshelvesSuccessAction
    | GetMyBookshelvesFailureAction
    | GetMyBooksAction
    | GetMyBooksSuccessAction
    | GetMyBooksFailureAction;
