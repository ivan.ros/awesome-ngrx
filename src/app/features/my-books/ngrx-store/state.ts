import { Bookshelf } from '@core/models/bookshelf';
import { Volume } from '@core/models/volume';

export interface State {
    bookshelves: Array<Bookshelf>;
    books: Array<Volume>;
    isLoading: boolean;
    error: string;
}

export const initialState: State = {
    bookshelves: [],
    books: [],
    isLoading: false,
    error: null
};
