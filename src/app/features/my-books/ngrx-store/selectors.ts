import { Bookshelf } from '@core/models/bookshelf';
import { Volume } from '@core/models/volume';
import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import { State } from './state';

const getError = (state: State): any => state.error;

const getIsLoading = (state: State): boolean => state.isLoading;

const getBookshelves = (state: State): any => state.bookshelves;

const getBooks = (state: State): any => state.books;

export const selectMyBooksState: MemoizedSelector<object, State> = createFeatureSelector<State>('myBooks');

export const selectMyBooksError: MemoizedSelector<object, any> = createSelector(selectMyBooksState, getError);

export const selectMyBooksIsLoading: MemoizedSelector<object, boolean> = createSelector(
    selectMyBooksState,
    getIsLoading
);

export const selectMyBooksBookshelves: MemoizedSelector<object, Array<Bookshelf>> = createSelector(
    selectMyBooksState,
    getBookshelves
);

export const selectMyBooksBooks: MemoizedSelector<object, Array<Volume>> = createSelector(selectMyBooksState, getBooks);
