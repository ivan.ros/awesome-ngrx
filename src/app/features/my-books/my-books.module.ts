import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { BooksModule } from '@shared/components/books/books.module';
import { MyBooksComponent } from './containers/my-books.component';
import { MyBooksEffects } from './ngrx-store/effects';
import { myBooksReducer } from './ngrx-store/reducer';

const routes = [
    {
        path: '',
        component: MyBooksComponent
    }
];

@NgModule({
    declarations: [MyBooksComponent],
    exports: [MyBooksComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        BooksModule,
        RouterModule.forChild(routes),
        StoreModule.forFeature('myBooks', myBooksReducer),
        EffectsModule.forFeature([MyBooksEffects]),
        MatProgressSpinnerModule,
        MatFormFieldModule,
        MatInputModule
    ]
})
export class MyBooksModule {}
