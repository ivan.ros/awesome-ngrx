import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MainPageComponent } from './containers/main-page.component';

const routes = [
    {
        path: '',
        component: MainPageComponent
    }
];

@NgModule({
    imports: [CommonModule, RouterModule.forChild(routes)],
    exports: [MainPageComponent],
    declarations: [MainPageComponent]
})
export class MainPageModule {}
